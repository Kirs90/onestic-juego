/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdv;

import java.io.*;
import java.util.Scanner;
import java.util.Arrays;
/**
 *
 * @author KristianSM
 */
public class JuegoDeVida {

    /**
     * @param args the command line arguments
     */
    public static Scanner sc = new Scanner(System.in);
    public static int filas = 10;
    public static int columnas = 10;
    public static int[][] TableroPrincipal = new int [10][10]; 
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Inicializamos la matriz de células
        // 0 = muerta, 1 = viva
             
        initTablero();
        
        // Número de iteraciones del juego
        
        int opcion;
        // Loop principal
        boolean jugar = true;
        String menu = "Elige una opción:\n1 - Empezar juego\n0 - Salir";
        System.out.println("BIENVENIDO AL JUEGO DE LA VIDA!\n");

        opcion = getInt(menu);

        switch (opcion){
            case 0: 
                jugar = false;
                System.out.println("¡Gracias por jugar!");
                break;
            case 1: 
            default:
                while (jugar)
                    jugar = juego();//Empieza el juego
                break;
        }               
    }
    
    public static void initTablero(){
        // En este método especificamos el estado inicial
        // de las células.
        int [][] tab = {
            {0,1,0,0,0,1,0,0,0,0},
            {0,0,1,0,0,0,0,0,0,1},
            {0,0,1,0,0,0,0,1,0,0},
            {1,0,0,0,0,0,0,1,1,0},
            {0,1,0,1,0,0,0,1,0,1},
            {1,0,0,0,0,1,0,0,1,0},
            {0,1,0,0,0,0,0,1,1,0},
            {0,1,1,0,0,0,1,0,1,1},
            {0,0,0,0,0,0,0,0,0,1},
            {0,1,1,0,0,1,0,0,1,0}};
        
        for (int i=0;i<10;i++)
            TableroPrincipal[i] = tab[i].clone();
    }
    
    public static boolean juego(){
        System.out.println("Este es el estado actual del tablero:\n");
        int iteraciones, opcion;
        mostrarTablero(TableroPrincipal);
        iteraciones = getIteraciones();        
        realizarIteraciones(iteraciones);
        opcion = getInt("¿Seguir jugando?\n1 - Si\n0 - No");
        if (opcion==0) return false;
        return true;
    }
    
    public static void mostrarTablero(int[][] tablero){
        
        for (int i=0;i<10;i++)
            System.out.println(Arrays.toString(tablero[i]));
    }
    
    public static int getIteraciones(){
        int res = 0;
        res = getInt("Especifica el número de iteraciones:");
        return res;
    }
    
    public static boolean realizarIteraciones(int iteraciones){
        System.out.println("¡EMPECEMOS!");
        for (int i=0; i < iteraciones; i++){
            System.out.println("ITERACIÓN "+(i+1));
            iteracion();
            mostrarTablero(TableroPrincipal);
        }
    return false;
    }
    
    public static void iteracion(){
        int i,j, numVivas;
        int[][] nuevoTablero = new int [10][10];
        //Podriamos llamar a un método para clonar el array original,
        // pero como igualmente lo vamos a recorrer, lo hacemos sobre la marcha
        
        // Para cada célula, contamos las vecinas vivas
        for (i=0;i<10;i++){
            for (j=0;j<10;j++){                
                numVivas = comprobarVivas(i,j);
                //System.out.println("("+i+ ")("+j+"): - "+numVivas);
                if (TableroPrincipal[i][j]==0){ // Si la célula actual está muerta
                    if (numVivas == 3)
                        nuevoTablero[i][j] = 1; //Nace
                    // En caso contrario copiamos el valor del array original,
                    // pero ya está inicializado a 0
                }
                else { // Si está viva
                    if (numVivas <2 || numVivas >3)
                        nuevoTablero[i][j] = 0; //Muere
                    else nuevoTablero[i][j] = 1; // Copiamos el valor original
                }
            }
        }
        
        TableroPrincipal = nuevoTablero.clone();
    }
    
    public static int comprobarVivas(int i, int j){
        int vivas = 0;
        //Fila superior
        if (i>0){
            if (j>0 && TableroPrincipal[i-1][j-1] == 1) vivas++;
            if (TableroPrincipal[i-1][j] == 1) vivas++;
            if (j<9 && TableroPrincipal[i-1][j+1] == 1) vivas++;
        }
        
        //Misma fila
        if (j>0 && TableroPrincipal[i][j-1] == 1) vivas++;
        if (j<9 && TableroPrincipal[i][j+1] == 1) vivas++;
        
        //Fila inferior
        if (i<9){
            if (j>0 && TableroPrincipal[i+1][j-1] == 1) vivas++;
            if (TableroPrincipal[i+1][j] == 1) vivas++;
            if (j<9 && TableroPrincipal[i+1][j+1] == 1) vivas++;
        }
        
        return vivas;
    }
    
    public static int getInt(String msg){
        int res=0;
        int valido = 0;
        
        while (valido==0){
            try{                
                if (!msg.equals(""))
                    System.out.println(msg);                
                res = sc.nextInt();
                valido=1;
            }
            catch(Exception e)
            {
                System.out.println("\nIntroduce un número entero positivo!\n");
                sc.next();
            }
        }
        return res;
    }
}
