Juego de la vida de Conway

Versi�n de consola en Java por Christian Senchermes

-En esta versi�n, hay un tablero de c�lulas de tama�o 10x10.

-El estado inicial del tablero se define dentro del mismo c�digo fuente,
en el m�todo "initTablero".*

-La aplicaci�n presenta un men� al jugador sobre si desea jugar o salir.

-Al jugar, se le pregunta al jugador el n�mero de iteraciones a realizar,
mostrando el resultado de cada una de estas.

-AL finalizar, se le pregunta al jugador si quiere seguir o salir.

*Esto ser�a m�s �ptimo hacerlo mediante el uso de un fichero externo que sirviese de entrada.
Sin embargo, como la aplicaci�n es muy "independiente" y la modificaci�n del c�digo fuente,
en este contexto, puede ser inmediata, he considerado que para comprobar la funcionalidad de
la aplicaci�n y la organizaci�n del c�digo, esto no era de lo m�s relevante.
